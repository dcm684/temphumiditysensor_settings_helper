import argparse
import serial
import json


def argument_parser():

    def valid_ip(inStr):
        """
        Verify that the given string is a valid IP

        Raises ArgumentTypeError if an invalid IP is given

        :param inStr: String to validate
        :return: Given string if valid
        """
        import socket
        try:
            socket.inet_aton(inStr)
            return inStr

        except socket.error:
            raise argparse.ArgumentTypeError

    def valid_port(inStr):
        """
        Verify that the given string is an int that can fit in 16-bits

        Raises ArgumentTypeError if an invalid IP is given

        :param inStr: String to validate
        :return: Int of the socket if valid
        """
        try:
            value = int(inStr)

        except ValueError:
            return argparse.ArgumentTypeError

        if value >= 2**16:
            return argparse.ArgumentTypeError

        return value

    def valid_ssid(inStr:str):
        """
        Verify that the given string is at most 32 characters long and does not contain any special characters

        Raises ArgumentTypeError if an invalid IP is given

        :param inStr: String to validate
        :return: SSID if valid
        """
        import re
        special_chars = re.compile('[\+\]\/\"\t]')
        leading_chars = re.compile('^[!#;]')

        if (len(inStr) > 32) \
                or (special_chars.search(inStr) is not None)\
                or (leading_chars.search(inStr) is not None)\
                or inStr.endswith(' '):
            return argparse.ArgumentTypeError

        return inStr

    parser = argparse.ArgumentParser(description="TempHumiditySensor settings program")

    call_reason_group = parser.add_mutually_exclusive_group(required='True')
    call_reason_group.add_argument('-u', '--upload',
                                   action='store_true',
                                   help="Upload new settings to the device")
    call_reason_group.add_argument('-d', '--download',
                                   action='store_true',
                                   help="Download existing settings from device")

    override_group = parser.add_argument_group('Overrides')
    override_group.add_argument('-s', '--ssid',
                                type=valid_ssid,
                                help="SSID of WiFi AP to connect")

    override_group.add_argument('-a', '--passphrase',
                                help="Passphrase of WiFi AP to connect")

    override_group.add_argument('-g', '--gateway_ip',
                                type=valid_ip,
                                help="IP of the network gateway")

    override_group.add_argument('-b', '--subnet_mask',
                                type=valid_ip,
                                help="Subnet IP Mask")

    override_group.add_argument('-v', '--server_ip',
                                type=valid_ip,
                                help="IP of the server")

    override_group.add_argument('-r', '--server_port',
                                type=valid_port,
                                help="Server port to connect to")

    override_group.add_argument('-c', '--client_ip',
                                type=valid_ip,
                                help="IP of the client")

    override_group.add_argument('-n', '--device_name',
                                help="Name to give the sensor")

    override_group.add_argument('-q', '--mqtt_prefix',
                                help="MQTT topic prefix")

    override_group.add_argument('-l', '--sleep',
                                help="Time in seconds to sleep between readings")

    parser.add_argument('-f', '--file',
                        action='store',
                        help="Location to load settings from or store them to depending on active function")

    parser.add_argument('-p', '--port',
                        action='store',
                        required=True,
                        help='Serial port used to communicate with device')

    return parser.parse_args()


class SerialInterface:
    COMMAND_DOWNLOAD = b'd'
    COMMAND_UPLOAD = b's'

    TYPE_SERVER_IP = 0xF0
    TYPE_SERVER_PORT = 0xF1
    TYPE_CLIENT_IP = 0xF2
    TYPE_SUBNET_IP = 0xF3
    TYPE_GATEWAY_IP = 0xF4
    TYPE_WIFI_SSID = 0xF5
    TYPE_WIFI_PASSPHASE = 0xF6
    TYPE_DEVICE_NAME = 0xF7
    TYPE_MQTT_TOPIC_PREFIX = 0xF8
    TYPE_SLEEP_TIME = 0xF9
    TYPE_EOT = 0xE0

    KEY_IP_SERVER = "ip_server"
    KEY_IP_CLIENT = "ip_client"
    KEY_IP_GATEWAY = "ip_gateway"
    KEY_IP_SUBNET = "ip_subnet"
    KEY_PORT_SERVER = "port_server"
    KEY_WIFI_SSID = "wifi_ssid"
    KEY_WIFI_PASSPHRASE = "wifi_passphrase"
    KEY_DEVICE_NAME = "device_name"
    KEY_MQTT_TOPIC_PREFIX = "mqtt_topic_prefix"
    KEY_SLEEP_TIME = "sleep"


    RESULT_SUCCESS = 0xDE
    RESULT_FAILURE = 0xAD

    type_ips = {TYPE_SERVER_IP, TYPE_CLIENT_IP, TYPE_SUBNET_IP, TYPE_GATEWAY_IP}
    type_strings = {TYPE_WIFI_SSID, TYPE_WIFI_PASSPHASE, TYPE_DEVICE_NAME, TYPE_MQTT_TOPIC_PREFIX}
    type_16bit = {TYPE_SERVER_PORT, TYPE_SLEEP_TIME}

    def __init__(self, port):
        self.serial_conn = serial.Serial()
        self.serial_conn.port = port
        self.serial_conn.baudrate = 115200
        self.serial_conn.timeout = 1.0

        self.reopen_serial_port()

    def __del__(self):
        self.serial_conn.close()

    def reset_device(self):
        """
        Resets the connect ESP8266
        :return: None
        """
        import time
        self.serial_conn.setDTR(False)
        time.sleep(1e-1)
        self.serial_conn.setDTR(True)

    def force_non_deep_sleep_wakeup(self):
        """
        When the reset button is pressed while in deep sleep the ESP8266 thinks that its waking from deep sleep

        If the reset button is pressed twice in quick succession, it will behave like its waking from a reset press
        :return: None
        """
        import time

        self.reset_device()
        time.sleep(1e-1) # Allow the reset handler to clear the previous reset
        self.reset_device()

    def reopen_serial_port(self):
        """
        Checks if the serial port is closed and if so reopens it
        :return: True if the port was closed
        """
        if not self.serial_conn.is_open:
            self.serial_conn.open()
            print("Opening serial port")

            self.force_non_deep_sleep_wakeup()

            import time
            time.sleep(1)
            self.serial_conn.flush()
            return True
        else:
            return False

    def compare_dicts(self, expected:dict, actual:dict) -> dict:
        """
        Compares the two dicts taking into consideration the type of data

        If one dict has a key and the other doesn't this will be considered to be a mismatch. If there is a key
        that is unknown, the values will be treated as strings

        :param expected: Dict with expected values
        :param actual: Dict with actual values
        :return: None if no differences, otherwise a dict with the offending key and an array of values
        """
        import ipaddress

        dict_of_errors = dict()
        actual_temp = actual.copy()

        for key, value in expected.items():
            if key not in actual_temp:
                dict_of_errors[key] = {expected[key], None}

            else:
                if key in self.type_ips:
                    if ipaddress.IPv4Address(expected[key]) != ipaddress.IPv4Address(actual_temp[key]):
                        dict_of_errors[key] = {expected[key], actual_temp[key]}

                elif key in self.type_16bit:
                    if int(expected[key]) != int(actual_temp[key]):
                        dict_of_errors[key] = {expected[key], actual_temp[key]}

                else:
                    # Treat everything else as strings
                    if expected[key] != actual_temp[key]:
                        dict_of_errors[key] = {expected[key], actual_temp[key]}

                try:
                    del actual_temp[key]

                except KeyError:
                    # Should never happen since it was already checked for
                    print("The key {} appeared magically in actual".format(key))


        return None

    @staticmethod
    def calculate_crc8(in_bytes: bytes):
        """
        Calculates the CRC8 value of the given bytes

        CRC-8 - based on the CRC8 formulas by Dallas/Maxim
        Based on microcontroller code

        :param in_bytes: Bytes to calculate from
        :return: CRC8 of the given bytes
        """

        out_crc = 0

        for byte in in_bytes:
            for i in range(8, 0, -1):
                crc_sum = (out_crc ^ byte) & 0x01
                out_crc >>= 1

                if crc_sum > 0:
                    out_crc ^= 0x8C

                byte >>= 1

        return out_crc

    @staticmethod
    def has_valid_crc(in_bytes: bytes):
        """
        Returns True if the given bytes have a valid CRC8 as the last byte

        :param in_bytes: Bytes to check with final byte as CRC8
        :return: True, if the bytes have a valid CRC8, False otherwise
        """
        return SerialInterface.calculate_crc8(in_bytes[:-1]) == in_bytes[-1]

    @staticmethod
    def get_string_with_in_crc_and_calculated_crc_and_prefix(prefix: str, in_bytes: bytes):
        """
        Returns a string with the prefix and the received CRC and calculated CRC8
        :param prefix: String to prepend out string with
        :param in_bytes: Bytes to calculate the CRC from. The last byte is the received CRC8
        :return: String with given prefix and CRC8 values
        """
        out_string = "{} In {:2X} Calc {:2X}".format(
                    prefix,
                    in_bytes[-1],
                    SerialInterface.calculate_crc8(in_bytes[:-1]))

        return out_string

    @staticmethod
    def process_ip_message(ip_message: bytes) -> str:
        """
        Processes the given IP message
        :param ip_message: Message containing an IP address with a CRC
        :return: String representing the read IP if valid, None otherwise
        """
        out_string = None

        if len(ip_message) == 5:

            if SerialInterface.has_valid_crc(ip_message):
                out_string = "{}.{}.{}.{}".format(
                    ip_message[0],
                    ip_message[1],
                    ip_message[2],
                    ip_message[3])

            else:
                print(SerialInterface.get_string_with_in_crc_and_calculated_crc_and_prefix(prefix="CRC Mismatch",
                                                                                           in_bytes=ip_message))

        else:
            print("IP address had an incorrect length, {}, expected 5 byte (4 IP + 1 CRC)".format(
                len(ip_message)
            ))

        return out_string

    @staticmethod
    def process_string_message(string_message: bytes):
        """
        Processes the given message containing a string
        :param string_message: Message containing a string with a CRC
        :return: Read string if CRC is valid and string is valid utf-8, None otherwise
        """

        out_string = None
        if SerialInterface.has_valid_crc(string_message):
            try:
                out_string = string_message[:-1].decode("utf-8")
            except UnicodeDecodeError:
                print("Non-utf8 string received")

        else:
            print(SerialInterface.get_string_with_in_crc_and_calculated_crc_and_prefix(prefix="CRC Mismatch",
                                                                                       in_bytes=string_message))

        return out_string

    @staticmethod
    def process_16bit_message(int16_bytes: bytes):
        """
        Processes the given message containing a 16-bit value

        16-bit values are expected to be little endian

        :param int16_bytes: Message containing a 16-bit value with a CRC
        :return: String representing the given 16-bit value if valid, None otherwise
        """

        out_string = None

        if len(int16_bytes) == 3:
            if SerialInterface.has_valid_crc(int16_bytes):
                out_string = int.from_bytes(int16_bytes[:-1], byteorder="little")

            else:
                print(SerialInterface.get_string_with_in_crc_and_calculated_crc_and_prefix(
                    prefix="CRC Mismatch",
                    in_bytes=int16_bytes))
        else:
            print("16-bit values message had incorrect length, {}, expected 3 bytes (2 port + 1 CRC)".format(
                len(int16_bytes)))

        return out_string

    def store_ip_in_dict(self, in_dict: dict, ip_type: int, ip_str: str):
        """
        Store the given IP string for in the appropriate location in the given dict
        :param in_dict: Dict to add the value too. If None, a new dict will be created
        :param ip_type: Type of IP address. If an invalid value is received the dict will remain unchanged
        :param ip_str: String to store as an IP. If None, nothing will be stored
        :return: None. in_dict is changed
        """

        if ip_str is None:
            return

        if in_dict is None:
            in_dict = {}

        if ip_type == self.TYPE_CLIENT_IP:
            in_dict[self.KEY_IP_CLIENT] = ip_str

        elif ip_type == self.TYPE_SERVER_IP:
            in_dict[self.KEY_IP_SERVER] = ip_str

        elif ip_type == self.TYPE_SUBNET_IP:
            in_dict[self.KEY_IP_SUBNET] = ip_str

        elif ip_type == self.TYPE_GATEWAY_IP:
            in_dict[self.KEY_IP_GATEWAY] = ip_str

    def store_string_in_dict(self, in_dict: dict, string_type: int, string_str: str):
        """
        Store the given string for in the appropriate location in the given dict
        :param in_dict: Dict to add the value too. If None, a new dict will be created
        :param string_type: Type of string. If an invalid value is received the dict will remain unchanged
        :param string_str: String to store. If None, nothing will be stored
        :return: None. in_dict is changed
        """

        if string_str is None:
            return

        if in_dict is None:
            in_dict = {}

        if string_type == self.TYPE_WIFI_SSID:
            in_dict[self.KEY_WIFI_SSID] = string_str

        elif string_type == self.TYPE_WIFI_PASSPHASE:
            in_dict[self.KEY_WIFI_PASSPHRASE] = string_str

        elif string_type == self.TYPE_DEVICE_NAME:
            in_dict[self.KEY_DEVICE_NAME] = string_str

        elif string_type == self.TYPE_MQTT_TOPIC_PREFIX:
            in_dict[self.KEY_MQTT_TOPIC_PREFIX] = string_str

    def store_port_in_dict(self, in_dict: dict, port_type: int, port_int: int):
        """
        Store the given port for in the appropriate location in the given dict
        :param in_dict: Dict to add the value too. If None, a new dict will be created
        :param port_type: Type of port message. If an invalid value is received the dict will remain unchanged
        :param port_int: Port to store. If None, nothing will be stored
        :return: None. in_dict is changed
        """

        if port_int is None:
            return

        if in_dict is None:
            in_dict = {}

        if port_type == self.TYPE_SERVER_PORT:
            in_dict[self.KEY_PORT_SERVER] = port_int

    def store_sleep_in_dict(self, in_dict: dict, time_in_seconds: int):
        """
        Stores the given time to sleep in the given dict
        :param in_dict: Dict to add the sleep time to
        :param time_in_seconds: Time in seconds to delay between readings
        :return: None. in_dict is modified
        """

        if time_in_seconds is None:
            return

        if in_dict is None:
            in_dict = {}

        in_dict[self.KEY_SLEEP_TIME] = time_in_seconds

    def request_and_process_settings_from_device(self):
        """
        Requests settings from the device and processes the response

        :return:Dict with the read settings. None if there was an error or nothing was received
        """

        self.reopen_serial_port()

        # Flush the queue
        self.serial_conn.flushInput()

        self.serial_conn.write(SerialInterface.COMMAND_DOWNLOAD)

        # Data response is
        # COMMAND_DOWNLOAD
        # Data1 Type
        # Data1
        # ...
        # DataN Type
        # DataN
        # EOT

        read_command = self.serial_conn.read(1)
        if read_command != SerialInterface.COMMAND_DOWNLOAD:

            print("Incorrect command received 0x{:02X}, Expected 0x {:02X}".format(
                int.from_bytes(read_command, "big"),
                int.from_bytes(SerialInterface.COMMAND_DOWNLOAD, "big")))

            print(self.serial_conn.read_all())

            # If an invalid response is received dump the data and exit
            self.serial_conn.flushInput()
            return

        data_dict = {}

        while True:
            data_type = int.from_bytes(self.serial_conn.read(1), byteorder="little")

            if data_type in self.type_ips:
                data = self.serial_conn.read(5)
                processed_ip = self.process_ip_message(data)
                self.store_ip_in_dict(data_dict, data_type, processed_ip)

            elif data_type in self.type_strings:
                string_length = int.from_bytes(self.serial_conn.read(1), byteorder="little")
                data = self.serial_conn.read(string_length + 1)
                processed_str = self.process_string_message(data)
                self.store_string_in_dict(data_dict, data_type, processed_str)

            elif data_type in self.type_16bit:
                data = self.serial_conn.read(3)
                processed_16bit_value = self.process_16bit_message(data)
                if data_type == self.TYPE_SERVER_PORT:
                    self.store_port_in_dict(data_dict, data_type, processed_16bit_value)
                elif data_type == self.TYPE_SLEEP_TIME:
                    self.store_sleep_in_dict(data_dict, processed_16bit_value)

            elif data_type == self.TYPE_EOT:
                break

            else:
                print("Invalid type 0x{:2X}. Burning all data in queue and exiting without making any changes.".format(
                    data_type))
                self.serial_conn.flushInput()
                data_dict = None
                break

        if len(data_dict) == 0:
            # Nothing was read so indicate to caller that invalid message occurred
            data_dict = None

        return data_dict

    def request_and_dump_to_file(self, path):
        """
        Requests all of the settings on the device and saves them to a file
        :param path File to store in
        :return: None
        """
        data_dict = self.request_and_process_settings_from_device()

        if data_dict is not None:
            with open(path, mode="w+") as out_file:
                json.dump(data_dict, out_file)

    def create_settings_dict(self, server_ip=None, server_port=None, client_ip=None, gateway_ip=None, subnet_ip=None,
                             wifi_ssid=None, wifi_passphrase=None, device_name=None, mqtt_prefix=None, sleep_time=None):
        """
        Returns a dict that can be used to upload settings to the hardware based on the given options
        :param server_ip: IP of the server for the hardware to connect to. If None, the dict will not contain the value.
        :param server_port: Port to connect to on the server. If None, the dict will not contain the value.
        :param client_ip: IP for the hardware. If None, the dict will not contain the value.
        :param gateway_ip: IP for the network gateway. If None, the dict will not contain the value.
        :param subnet_ip: IP mask for the subnet. If None, the dict will not contain the value.
        :param wifi_ssid: SSID of the WLAN. If None, the dict will not contain the value.
        :param wifi_passphrase: Passphrase from the WLAN. If None, the dict will not contain the value.
        :param device_name: Name to give the ESP8266 device
        :param mqtt_prefix: MQTT topic prefix
        :param sleep_time: Time in seconds to sleep between readings
        :return: Dict that can be used ot upload to the hardware. If all values are None, the dict will be empty
        """

        out_dict = dict()

        if server_ip is not None:
            out_dict[self.KEY_IP_SERVER] = server_ip

        if server_port is not None:
            out_dict[self.KEY_PORT_SERVER] = server_port

        if client_ip is not None:
            out_dict[self.KEY_IP_CLIENT] = client_ip

        if gateway_ip is not None:
            out_dict[self.KEY_IP_GATEWAY] = gateway_ip

        if subnet_ip is not None:
            out_dict[self.KEY_IP_SUBNET] = subnet_ip

        if wifi_ssid is not None:
            out_dict[self.KEY_WIFI_SSID] = wifi_ssid

        if wifi_passphrase is not None:
            out_dict[self.KEY_WIFI_PASSPHRASE] = wifi_passphrase

        if device_name is not None:
            out_dict[self.KEY_DEVICE_NAME] = device_name

        if mqtt_prefix is not None:
            out_dict[self.KEY_MQTT_TOPIC_PREFIX] = mqtt_prefix

        if sleep_time is not None:
            out_dict[self.KEY_SLEEP_TIME] = sleep_time

        return out_dict


    def load_file_and_upload(self, path:str=None, arg_dict:dict=None) -> dict:
        """
        Loads the given file and uploads the stored settings to the device
        :param path: Path of the file to load.
        :param arg_dict: Dictionary of settings that will override those in the file. Most often these are values
        set via the command line args
        :return: Dictionary of values that were uploaded. None if nothing was uploaded
        """

        data_dict = dict()

        # When there is a file specified, load it
        if path is not None:

            try:
                with open(path, mode="r") as in_file:
                    data_dict = json.load(in_file)

            except FileNotFoundError:
                print("Log {} file does not exist".format(path))

        # Copy the values from the argument dictionary into the dict to be
        # uploading and replacing any existing values
        data_dict.update(arg_dict)

        print("Uploaded data:")
        import pprint
        pprint.pprint(data_dict)

        if data_dict is None:
            return None

        else:
            out_messages = []
            for a_key in data_dict:
                message = SerialInterface.get_message_to_send_for_key(self, data_dict, a_key)

                if message is not None:
                    out_messages.append(message)

            self.upload_list_of_settings_messages(out_messages)

        return data_dict

    def get_message_to_send_for_key(self, in_dict:dict, in_key:str) -> bytes:
        """
        Takes the given key and uses its value in the dict to create a message to send to the module
        :param in_dict: Dict containing the key and data
        :param in_key: Data to find
        :return: Message for the given key. None if the key is not in the dict
        """
        data_type = None

        if in_key in in_dict:

            if in_key == self.KEY_WIFI_SSID:
                data_type = self.TYPE_WIFI_SSID
            elif in_key == self.KEY_WIFI_PASSPHRASE:
                data_type = self.TYPE_WIFI_PASSPHASE

            elif in_key == self.KEY_PORT_SERVER:
                data_type = self.TYPE_SERVER_PORT

            elif in_key == self.KEY_IP_SERVER:
                data_type = self.TYPE_SERVER_IP
            elif in_key == self.KEY_IP_CLIENT:
                data_type = self.TYPE_CLIENT_IP
            elif in_key == self.KEY_IP_GATEWAY:
                data_type = self.TYPE_GATEWAY_IP
            elif in_key == self.KEY_IP_SUBNET:
                data_type = self.TYPE_SUBNET_IP

            elif in_key == self.KEY_DEVICE_NAME:
                data_type = self.TYPE_DEVICE_NAME

            elif in_key == self.KEY_MQTT_TOPIC_PREFIX:
                data_type = self.TYPE_MQTT_TOPIC_PREFIX

            elif in_key == self.KEY_SLEEP_TIME:
                data_type = self.TYPE_SLEEP_TIME

        if data_type is None:
            return None

        if data_type in self.type_ips:
            data_bytes = SerialInterface.create_ip_message(in_dict[in_key])
            # print("IP: {} {}".format(in_key, data_bytes))

        elif data_type in self.type_strings:
            data_bytes = SerialInterface.create_string_message(in_dict[in_key])
            # print("String: {} {}".format(in_key, data_bytes))

        elif data_type in self.type_16bit:
            data_bytes = SerialInterface.create_16bit_int_message(in_dict[in_key])
            # print("Port: {} {}".format(in_key, data_bytes))

        else:
            raise Exception("Invalid data type {}".format(data_type))

        out_bytes = bytes([data_type]) + data_bytes

        return out_bytes

    @staticmethod
    def create_ip_message(ip_string: str) -> bytes:
        """
        Converts the given string to an IP message without the type
        :param ip_string: String to convert. String must be of the format XXX.XXX.XXX.XXX. Each octet can have 1-3 chars
        :return: IP message with checksum. None if the IP string is invalid
        """

        out_bytes = None

        octets = ip_string.split('.')

        if len(octets) == 4:
            out_ints = list(map(int, octets))

            out_bytes = bytes(out_ints)

            crc_int = SerialInterface.calculate_crc8(out_bytes)

            out_ints.append(crc_int)

            out_bytes = bytes(out_ints)

        return out_bytes

    @staticmethod
    def create_string_message(string_string: str) -> bytes:
        """
        Converts the given string to string message without the type
        :param string_string: String to convert to bytes
        :return: String message with checksum.
        """
        string_bytes = bytearray(string_string, encoding="utf-8")

        crc_int = SerialInterface.calculate_crc8(string_bytes)

        string_bytes.append(crc_int)

        # First byte needs to be length
        string_bytes[0:0] = [len(string_string)]

        return bytes(string_bytes)

    @staticmethod
    def create_16bit_int_message(in_int: int) -> bytes:
        """
        Converts the given int to port message without the type
        :param in_int: Int to convert to bytes. Must be less than or equal to 65535
        :return: Int message with checksum. None if the int value is greater than 65535
        """

        if in_int > 65535:
            return None

        import struct

        # Value is little endian with 2 bytes
        int_bytes = struct.pack("<h", in_int)
        int_bytes = bytearray(int_bytes)

        crc_int = SerialInterface.calculate_crc8(int_bytes)

        int_bytes.append(crc_int)

        return bytes(int_bytes)

    def upload_list_of_settings_messages(self, messages_to_send:list):
        """
        Send the list of settings to the hardware.

        Once all data has been sent an EOT byte is sent to tell hardware transmission is complete.

        :param messages_to_send: List of messages to send
        :return: True if all of the data was sent
        """

        message_success = True

        if len(messages_to_send) <= 0:
            # All 0 bytes of data were sent
            return True

        self.reopen_serial_port()

        # Flush the queue
        self.serial_conn.flushInput()

        self.serial_conn.write(SerialInterface.COMMAND_UPLOAD)

        response = self.serial_conn.read(1)
        if response != SerialInterface.COMMAND_UPLOAD:
            print("Invalid response to starting upload: ", end="")
            print(response)
            print("Resetting module then retrying")
            self.force_non_deep_sleep_wakeup()

            response = self.serial_conn.read(1)
            if response != SerialInterface.COMMAND_UPLOAD:
                print("Invalid response to starting upload: ", end="")
                print(response)
                print("Failing out")
                return False

        for a_message in messages_to_send:
            self.serial_conn.write(a_message)
            # print("Sending ", end="")
            # print(a_message)

            # Wait for the response. (Message Type, Result)
            response = self.serial_conn.read(2)
            if len(response) != 2:
                print("Response of invalid length received. In message length {}".format(len(response)))
                message_success = False

            elif response[0] != a_message[0]:
                print("Invalid message type: {:02X}, sent {:02X}".format(
                    response[0], a_message[0]))
                message_success = False

            elif response[1] != self.RESULT_SUCCESS:
                print("RESULT_SUCCESS not received. Received {:02X}".format(response[1]))

            # else:
            #     print("Message was successfully sent {:02X}".format(a_message[0]))

        self.serial_conn.write([self.TYPE_EOT])
        response = self.serial_conn.read(1)

        if len(response) == 0:
            print("No response was received")
            message_success = False
        elif response[0] != self.TYPE_EOT:
            second_response = self.serial_conn.read(1)
            print("EOT was not handled as expected {:02X} {:02X}".format(response[0], second_response[0]))
            message_success = False
        # else:
        #     print("EOT was received")

        return message_success


if __name__ == "__main__":
    args = argument_parser()

    interface = SerialInterface(port=args.port)

    if args.upload:

        arg_dict = interface.create_settings_dict(server_ip=args.server_ip,
                                                  server_port=args.server_port,
                                                  client_ip=args.client_ip,
                                                  gateway_ip=args.gateway_ip,
                                                  subnet_ip=args.subnet_mask,
                                                  wifi_ssid=args.ssid,
                                                  wifi_passphrase=args.passphrase,
                                                  device_name=args.device_name,
                                                  mqtt_prefix=args.mqtt_prefix)

        uploaded = interface.load_file_and_upload(args.file, arg_dict)

        if uploaded is None:
            print("Failed to upload")

        else:
            on_device = interface.request_and_process_settings_from_device()

            if on_device is None:
                print("Failed to receive verification data")

            else:
                differences = interface.compare_dicts(expected=uploaded, actual=on_device)

                if differences is None:
                    print("Successfully uploaded data")

                else:
                    print("Failed to upload data successfully. Differences")
                    print(differences)

    elif args.download:

        if args.file is None:
            # If no file is provided, the current settings are printed
            print(interface.request_and_process_settings_from_device())

        else:
            # When a file is provided, store to the file
            interface.request_and_dump_to_file(args.file)

    else:
        # Should be impossible to reach if argument_parser() does its work
        print("Upload and download args are both false.")

    exit()
